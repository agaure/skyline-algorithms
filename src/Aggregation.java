import myutil.*;
import java.util.*;

public class Aggregation {
	public static void main(String[] args){
		Data data = new Data();
		long startTime = System.currentTimeMillis();
		ArrayList<ArrayList<float[]>> attr = arrangeData(data);
		ArrayList<float[]>skylines = new ArrayList<float[]>();
		ArrayList<float[]>partials = new ArrayList<float[]>();		
		attr = dataSort(attr);
		 int[] count = new int[attr.get(0).size()];
		 boolean flag = false;
		 for(int i = 0; i < attr.get(0).size(); i++){
			 for(int j= 0; j < data.query.dimen.size(); j++){
				 int p = (int)((attr.get(j).get(i))[0]) - 1;
				 if(i == 0){
					 if(!isAdded(skylines, p))
						 skylines.add(data.points.get(p));				 
				 }else{
					 if(!isAdded(skylines, p)){
						 if(!isAdded(partials, p))
							 partials.add(data.points.get(p));
					 }
				 }
				 count[p]++;
				 if(count[p] == data.query.dimen.size()){
					 if(!isAdded(skylines, p))
						 skylines.add(data.points.get(p));
					 flag = true;
					 break;
				 }
			 }
			 if(flag){
				 break;
			 }
		 }
		 ListIterator piter = partials.listIterator();
		 while(piter.hasNext()){
			 float[] pele =  (float[])piter.next();
			 ArrayList<Float> yetDomin = new ArrayList<Float>();
			 for(ArrayList<float[]> sublist : attr){
				 for(float[] tup : sublist){
					 if(tup[0] == pele[0])
						 break;
					 else{
						 yetDomin.add(tup[0]);
					 }
				 }
			 }
			 int[] totDomin = new int[data.points.size()];
			 boolean isDom = false;
			 for(float yd : yetDomin){
				 int t = (int)(yd-1);
				 totDomin[t]++;
				 if(totDomin[t] == data.query.dimen.size())
					 isDom = true;
			 }
			 if(!isDom){
				 if(!isAdded(skylines, (int)pele[0]-1))
				 skylines.add(data.points.get((int)pele[0]-1));
			 }				 
		 }
   		 long runTime = System.currentTimeMillis() - startTime;
		 Impure.printRes(skylines, runTime);
	}
	
	public static ArrayList<ArrayList<float[]>> arrangeData(Data data){
		ArrayList<ArrayList<float[]>> attr = new ArrayList<ArrayList<float[]>>();
		ListIterator diter = data.points.listIterator();
		while(diter.hasNext()){
			float[] x = (float[]) diter.next();
			int i = 0;
			ListIterator qiter = data.query.dimen.listIterator();
			while(qiter.hasNext()){
				int q = (int) qiter.next();
				float[] row = new float[2];
				row[0] = x[0];
				row[1] = x[q];
				if(x[0]==1){
					ArrayList<float[]> lst = new ArrayList<float[]>();
					attr.add(lst);
				}
				attr.get(i).add(row);
				i++;
			}			
		}
		return attr;
	}
	
	public static ArrayList<ArrayList<float[]>> dataSort(ArrayList<ArrayList<float[]>> attr){
		ListIterator aiter = attr.listIterator();
		while(aiter.hasNext()){
			ArrayList<float[]> x = (ArrayList<float[]>)aiter.next();
			Collections.sort(x, new Comparator<float[]>() {
	            public int compare(float[] point1, float[] point2) {
	                return Float.compare(point1[1], point2[1]);
	            }
	        });
		}
		return attr;
	}
	
	public static boolean isAdded(ArrayList<float[]> skylines, int p){
		boolean added = false;
		for(int k = 0; k < skylines.size(); k++){
			 added = added || ((int)(skylines.get(k))[0] - 1 == p);						 
		}
		return added; 
	}
}