package myutil;

import java.util.*;

public class Window {
	public ArrayList<float[]> win = new ArrayList<float[]>();
	private int winSize;
	private int filled = 0;
	
	public Window(int ws){
		winSize = ws;
	}
	
	public void addWin(float[] x){
		win.add(x);
		filled++;
	}
	
	public void rmWin(int ind){
		win.remove(ind);
		filled--;
	}
	
	public void clearWin(){
		win.clear();
		filled = 0;
	}
	
	public boolean isfull(){
		if(filled == winSize)
			return true;
		else 
			return false;
	}
	
	public boolean isempty(){
		if(filled == 0)
			return true;
		else 
			return false;
	}

}