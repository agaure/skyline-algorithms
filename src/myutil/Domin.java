package myutil;
import java.util.*;

public class Domin {
	public static int count = 0;
	public static boolean isDominate(float[] p1, float[] p2, Query q){
		boolean cond1 = true;
		boolean cond2 = false;
		ListIterator qiter = q.dimen.listIterator(); 
		while(qiter.hasNext()){
			int i = (int)qiter.next();
			cond1 = cond1 && (p2[i] <= p1[i]);
			cond2 = cond2 || (p2[i] < p1[i]);
		}
		
	/*	//----------------------------------
		if(p1[0] == 309.0)
			System.out.print(p2[0] + " ");
		if(p2[0] == 309.0)
			System.out.print(p1[0] + " ");
		//---------------------------------- */
		count++;
		return cond1 && cond2;
	}
}
