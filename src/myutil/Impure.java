package myutil;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Impure {
	//ArrayList<String[]> testCases = new ArrayList<String[]>();
	public static String [][] testCases = {{"input1.txt", "query1.txt"},
										   {"input2.txt", "query2.txt"},
										   {"sample_ant.txt", "sample_query.txt"},
										   {"sample_cor.txt", "sample_query.txt"},
										   {"sample_ind.txt", "sample_query.txt"}};
	public static String[] testCase = testCases[4];
	public static ArrayList<float[]> getinput() throws IOException{
		Scanner in = null;
		ArrayList<float[]> input = new ArrayList<float[]>(); 
		String[] textRow;
		float[] row;
		int n = 1, m = 0;
		try{
			in = new Scanner(new File(testCase[0]));
			in.nextLine();
			int c = 0;
			while(in.hasNextLine()){
				textRow = (in.nextLine()).split("\\s+");
				row = new float[textRow.length];
				for(int i = 0; i < textRow.length; i++){
					row[i] = Float.parseFloat(textRow[i]);
				}
				input.add(row);
				c++;
			}
		}
		finally{
			if(in != null){
				in.close();
			}
		}
		return input;
	}
	
	public static Query getQuery() throws IOException{
		Scanner in = null;
		ArrayList<Integer> dimen = new ArrayList<Integer>(); 
		int WinSize;
		try{
			in = new Scanner(new File(testCase[1]));
			String[] textarr;
			textarr = (in.nextLine().split("\\s+"));
			for(int i = 0; i < textarr.length; i++)
				dimen.add(Integer.parseInt(textarr[i]));
			WinSize = in.nextInt();
		}
		finally{
			in.close();
		}
		Query query1 = new Query();
		query1.WinSize = WinSize;
		query1.dimen = dimen;
		return query1;
	}
	
	public static void printRes(List<Integer> SkyLineSet, long t){
		System.out.println("Running Time: " + t + " milli sec.");
		System.out.println("No. of Comparisions: " + Domin.count);
		System.out.println("Skyline Set Size: "+SkyLineSet.size());
		System.out.print("Skyline Set: ");
		for(int x: SkyLineSet){
			System.out.print(x+" ");
		}
	}
	
	public static void printRes(ArrayList<float[]> SkylineSet, long t){
		ListIterator litr = SkylineSet.listIterator();
		List<Integer> sky = new ArrayList<Integer>();
		while(litr.hasNext()){
			sky.add((int)((float[])litr.next())[0]);
		}
		printRes(sky, t);
	}
}	