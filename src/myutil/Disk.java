package myutil;
import java.util.*;

public class Disk{
	int fileSize = 0;
	public ArrayList<float[]> diskFile = new ArrayList<float[]>();
	
	public void addDisk(float[] x){
		diskFile.add(x);
		fileSize++;
	}
	
	public void clearDisk(){
		diskFile.clear();
		fileSize = 0;
	}
}

