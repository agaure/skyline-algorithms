import java.util.*;
import myutil.*;

public class Naive {
	public static void main(String[] args){
		Data data = new Data();
		ArrayList<Integer> dimen = data.query.dimen;
		List<Integer> SkyLineSet = new ArrayList<Integer>();
		boolean isSkyline;
		//Start Timer-------------------
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < data.points.size(); i++){
			isSkyline = true;
			for(int j = 0; j < data.points.size(); j++){
				if(Domin.isDominate(data.points.get(i), data.points.get(j), data.query)){
					isSkyline = false;
					break;
				}
			}
			if(isSkyline){
				SkyLineSet.add((int)data.points.get(i)[0]);
			}
		}
		//Start Timer-------------------
		long endTime = System.currentTimeMillis();
		long runTime = endTime - startTime; 
		Impure.printRes(SkyLineSet, runTime);
	}	
}